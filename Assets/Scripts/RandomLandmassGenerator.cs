using UnityEngine;

public class RandomLandmassGenerator : MonoBehaviour {

	public int mapWidth;
	public int mapHeight;
	public float noiseScale;
	public int ocatves;
	[Range(0, 1)]
	public float persistance;
	public float lacunarity;
	public Vector2 offset;

	public bool useFalloff = true;
	float[,] mFalloffMap;

	public Vector2 heightRange;
	public Vector2 heightCurveRange;

	private AnimationCurve heightCurve;

	// public float heightMultiplier;

	public void Awake() {
		mFalloffMap = FalloffGenerator.GenerateFalloffMap(mapWidth);
	}

	public void Start() {
		GenerateLandMass();

	}

	public void GenerateLandMass() {
		var seedRandom = new System.Random(SeedManager.Seed);

		float[,] noiseMap = Noise.GenerateNoiseMap(mapWidth,
													mapHeight,
													SeedManager.Seed,
													noiseScale,
													ocatves,
													persistance,
													lacunarity,
													offset
												);
		heightCurve = new AnimationCurve();

		var r = (float)seedRandom.NextDouble();
		var tangent = Mathf.Lerp(heightCurveRange.x, heightCurveRange.y, r);
		heightCurve.AddKey(new Keyframe(0, 0, tangent, tangent));
		r = (float)seedRandom.NextDouble();
		tangent = Mathf.Lerp(heightCurveRange.x, heightCurveRange.y, r);
		heightCurve.AddKey(new Keyframe(1, 1, tangent, tangent));

		r = (float)seedRandom.NextDouble();
		var heightScalar = Mathf.Lerp(heightRange.x, heightRange.y, r);

		if (useFalloff) {
			for (int y = 0; y < mapHeight; y++) {
				for (int x = 0; x < mapWidth; x++) {
					noiseMap[x, y] -= mFalloffMap[x, y];
					noiseMap[x, y] = Mathf.Clamp01(noiseMap[x, y]);
					noiseMap[x, y] = heightCurve.Evaluate(noiseMap[x, y]);
					// noiseMap[x, y] = fixedHeightCurve.Evaluate(noiseMap[x, y]);
				}
			}
		}

		Debug.Log(heightCurve.keys[0].inTangent + " " + heightCurve.keys[0].outTangent);
		Debug.Log(heightCurve.keys[1].inTangent + " " + heightCurve.keys[1].outTangent);

		MeshData meshData = MeshGenerator.GenerateTerrainMesh(noiseMap, heightScalar);
		meshData = MeshGenerator.SplitMeshDataTriangles(meshData);
		Mesh mesh = meshData.CreateMesh();
		GetComponent<MeshFilter>().sharedMesh = mesh;
		GetComponent<MeshCollider>().sharedMesh = mesh; 
	}

	void OnValidate() {
		if (mapWidth < 1) {
			mapWidth = 1;
		}
		if (mapHeight < 1) {
			mapHeight = 1;
		}
		if (lacunarity < 1) {
			lacunarity = 1;
		}
		if (ocatves < 0) {
			ocatves = 0;
		}
	}
}
