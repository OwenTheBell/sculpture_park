using UnityEngine;
using System.Collections;

public class FadeAfterSecondsScript : MonoBehaviour {

	public float waitTime;
	public float fadeTime;

	void Start() {
		StartCoroutine(Fade());
	}

	IEnumerator Fade() {
		yield return new WaitForSeconds(waitTime);

		var time = 0f;
		while (time < fadeTime) {
			time += Time.deltaTime;
			var easeInOut = Easing.EaseInOut(time/fadeTime, 2);
			var color = Color.white;
			if (GetComponent<Renderer>()) {
				color = GetComponent<Renderer>().material.color;
				color.a = 1 - easeInOut;
				GetComponent<Renderer>().material.color = color;
			}
			else if (GetComponent<CanvasRenderer>()) {
				GetComponent<CanvasRenderer>().SetAlpha(1 - easeInOut);
			}

			yield return 0;
		}

		yield return null;
	}

}
