using UnityEngine;

public class SeedReceiver : MonoBehaviour {

	public int seed;

	public void Awake() {
		SeedManager.Seed = seed;
	}
}
