using UnityEngine;
using UnityEngine.UI;

public class InputStringProcessor : MonoBehaviour {

	private Text _text;
	public int MaxCharacters;

	public void Awake() {
	}

	public void Start() {
		_text = GetComponent<Text>();
	}

	public void Update() {
		if (Input.inputString.Length == 0) {
			return;
		}
		foreach (char c in Input.inputString) {
			/* handle backspace */
			if (c == "\b"[0] && _text.text.Length > 0) {
				_text.text = _text.text.Substring(0, _text.text.Length - 1);
			}
			/* catch enter key presses and ignore them */
			else if (c == "\n"[0] || c == "\r"[0]) {
				/* another script handles processing the seed */
				// Debug.Log("park name is: " + _text.text);
			}
			else if (_text.text.Length < MaxCharacters) {
				_text.text += c;
			}
		}
	}
}
