using UnityEngine;

public class SplitMeshTrianglesScript : MonoBehaviour {
	
	public void Start() {
		Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
		mesh = MeshGenerator.SplitMeshTriangles(mesh);
		GetComponent<MeshFilter>().sharedMesh = mesh;
	}
}
