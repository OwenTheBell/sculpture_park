using UnityEngine;

public class QuitScript : MonoBehaviour {

	public KeyCode quitKey;

	void Update() {
		if (Input.GetKeyDown(quitKey)) {
			Application.Quit();
		}
	}
}
