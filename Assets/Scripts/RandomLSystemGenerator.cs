using UnityEngine;
using System.Collections.Generic;

public class RandomLSystemGenerator : MonoBehaviour {

	public int MaxLSystemIterations = 2;
	public Vector2 procedureRange = new Vector2(15, 25);
	public Vector2 fRange = new Vector2(5, 10);

	private LSystem _LSystem;

	public void Start() {
		var operations = new char[]{'+', '-', '/', '\\', '&', '*'};


		var globalSeedRandom = new System.Random(SeedManager.Seed);

		var baseProcedure = new List<char>();

		var baseFCount = globalSeedRandom.Next(2, 6);
		for (int i = 0; i < baseFCount; i++) {
			baseProcedure.Add('F');
		}

		var baseOperationsCount = globalSeedRandom.Next(0, 4);
		for (int i = 0; i < baseOperationsCount; i++) {
			int operation_index = globalSeedRandom.Next(0, baseProcedure.Count - 1);
			while (baseProcedure[operation_index] == ']' || baseProcedure[operation_index] == '[') {
				operation_index++;
				if (operation_index >= baseProcedure.Count - 1) {
					operation_index = 0;
				}
			}
			char operation = operations[globalSeedRandom.Next(0, operations.Length - 1)];
			baseProcedure.Insert(operation_index, operation);
		}

		var procedureString = "";
		foreach(char c in baseProcedure) {
			procedureString += c;
		}
		Debug.Log("base procedure: " + procedureString);

		var seed = 0f;
		for (int i = 0; i < 3; i++) {
			seed += transform.position[i];
		}
		seed *= 100;

		var seedRandom = new System.Random((int)seed);

		int length = seedRandom.Next((int)procedureRange.x, (int)procedureRange.y);

		/*
		* first populate the procedure with Fs. This ensures that there are
		* at least a minimum amount in the procedure. The plants just don't look
		* good if there aren't enough Fs.
		*/
		List<char> newProcedure = new List<char>();

		newProcedure.AddRange(baseProcedure);
		length -= baseProcedure.Count;

		int F_count = seedRandom.Next((int)fRange.x, (int)fRange.y);
		for (int i = 0; i < F_count; i++) {
			newProcedure.Add('F');
		}

		// Debug.Log("populated Fs");

		/*
		* Populate with [ ] pairs. This is done second to ensure that all pairs
		* include at least one F inside them.
		* The total number of brackets needs to be limited by the number of Fs in
		* the system or it is possible for an impossible number of brackets to be
		* fit into the system.
		*/
		int max_brackets = 4;
		if (newProcedure.Count <= 10) {
			max_brackets = Mathf.RoundToInt(newProcedure.Count / 3f);
		}

		int bracket_count = max_brackets;
		if (bracket_count > 2) {
			bracket_count = seedRandom.Next(2, max_brackets);
		}
		int iterations = max_brackets;
		if (iterations > 2) {
			iterations = seedRandom.Next(2, max_brackets);
		}
		for (int i = 0; i < iterations; i++) {
			int index = seedRandom.Next(0, newProcedure.Count - 2);
			int checkBehind = (index > 0) ? index - 1 : 0;
			int checkAhead = index + 1;
			while (newProcedure[checkBehind] == '[' || newProcedure[checkAhead] == '[') {
				index++;
				checkAhead++;
				checkBehind++;
				if (index == newProcedure.Count - 1) {
					index = 0;
					checkBehind = 0;
					checkAhead = 1;
				}
			}
			newProcedure.Insert(index, '[');
		}

		// Debug.Log("placed open brackets");

		for (int i = 0; i < newProcedure.Count - 1; i++) {
			if (newProcedure[i] == '[') {
				int close_index = seedRandom.Next(i + 2, newProcedure.Count - 1);
				while(newProcedure[close_index - 1] == '[') {
					close_index++;
				}
				newProcedure.Insert(close_index, ']');
			}
		}

		// Debug.Log("placed close brackets");

		/*
		* insert operations between the characters.
		* Do not put any at the very end as they will have no effect on the 
		* interpretation.
		* Also ensure that all operations are not directly to the left of brackets
		* as then they will have no effect on the system.
		*/ 
		int operations_count = length - F_count - bracket_count;
		for (int i = 0; i < operations_count; i++) {
			int operation_index = seedRandom.Next(0, newProcedure.Count - 1);
			while (newProcedure[operation_index] == ']' || newProcedure[operation_index] == '[') {
				operation_index++;
				if (operation_index >= newProcedure.Count - 1) {
					operation_index = 0;
				}
			}
			char operation = operations[seedRandom.Next(0, operations.Length - 1)];
			newProcedure.Insert(operation_index, operation);
		}

		var procedures = new Dictionary<string, string>();
		string procedure = "";
		foreach(char c in newProcedure) {
			procedure += c;
		}
		procedures.Add("F", procedure);
		_LSystem = new LSystem("F", procedures);

		int n = seedRandom.Next(2, MaxLSystemIterations);

		GetComponent<TurtleInterpreter>().InterpretLSystem(_LSystem.ExpandedSystems[n]);
	}
}
