using UnityEngine;
using System.Collections.Generic;

public static class PointDistributer {

	public static Vector3[] Mitchells(float width, float height, int count, int seed) {
		System.Random seededRandom = new System.Random(seed);
		var samples = new List<Vector3>();

	    while (samples.Count < count) {
	    	var newSample = Vector3.zero;
	    	for (int i = 0; i < 10; i++) {
	    		var test = new Vector3(
		    							(float)seededRandom.NextDouble() * width - width/2f,
		    							0f,
		    							(float)seededRandom.NextDouble() * height - height/2f
	    							);
			    var smallest = Vector3.zero;
			    var furthestDistance = 0f;
			    foreach (Vector3 sample in samples) {
			        var distance = Vector3.Distance(sample, test);
			        if(distance > furthestDistance){
			            newSample = test;
			            furthestDistance = distance;
			        }
			    }
	    	}
		    samples.Add(newSample);
		}

		return samples.ToArray();
	}
}
