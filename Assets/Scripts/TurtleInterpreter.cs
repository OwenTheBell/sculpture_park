﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public struct TurtleState {
	public Vector3 position;
	public Quaternion rotation;

	public TurtleState(Vector3 pos, Quaternion rot) {
		position = pos;
		rotation = rot;
	}
}

public class TurtleInterpreter : MonoBehaviour {

	public float moveDistance = 1f;
	public float delta = 27.5f;
	public float radius = 0.1f;

	public GameObject turtleCylinder;

	private Stack<TurtleState> turtleStack = new Stack<TurtleState>();
	private List<TurtleState> turtleHistory = new List<TurtleState>();
	public List<TurtleState> TurtlePath {
		get {
			return turtleHistory;
		}
	}

	private LSystemHandler lSystem;


	void Awake() {
		lSystem = GetComponent<LSystemHandler>();
	}


	// Use this for initialization
	void Start () {
		
	}
	

	// Update is called once per frame
	void Update () {
	
	}


	public void InterpretLSystem(string system) {
		InterpretLSystem(system.ToCharArray());
	}


	public void InterpretLSystem(char[] system) {
		turtleHistory.Clear();
		TurtleState turtle = new TurtleState(Vector3.zero, Quaternion.LookRotation(Vector3.up));
		Quaternion rotation;
		foreach (char command in system) {
			switch (command) {
				case 'F':
					turtleHistory.Add(turtle);
					//drop down to f since it also moves the turtle forward
					goto case 'f';
					break;
				case 'f':
					Vector3 pos = Vector3.forward * moveDistance;
					pos = turtle.rotation * pos;
					turtle.position += pos;
					break;
				case '+':
					rotation = Quaternion.AngleAxis(delta, Vector3.up);
					turtle.rotation *= rotation;
					break;
				case '-':
					rotation = Quaternion.AngleAxis(-delta, Vector3.up);
					turtle.rotation *= rotation;
					break;
				case '[':
					turtleStack.Push(turtle);
					break;
				case ']':
					if (turtleStack.Count > 0) {
						turtle = turtleStack.Pop();
					} else {
						Debug.Log("<color=red>unable to pop turtle</color>");
					}
					break;
				case '&':
					rotation = Quaternion.AngleAxis(delta, Vector3.left);
					turtle.rotation *= rotation;
					break;
				case '^':
					rotation = Quaternion.AngleAxis(delta, Vector3.left);
					turtle.rotation *= rotation;
					break;
				case '\\':
					rotation = Quaternion.AngleAxis(delta, Vector3.forward);
					turtle.rotation *= rotation;
					break;
				case '/':
					rotation = Quaternion.AngleAxis(-delta, Vector3.forward);
					turtle.rotation *= rotation;
					break;
				case '|':
					rotation = Quaternion.AngleAxis(180, Vector3.up);
					turtle.rotation *= rotation;
					break;
			}
		}
		AssemblePlant();
	}


	void AssemblePlant() {
		int vertexCount = 0;
		List<CombineInstance> toCombine = new List<CombineInstance>();
		foreach(TurtleState turtle in turtleHistory) {
			GameObject cylinder;
			if (turtleCylinder) {
				cylinder = Instantiate(turtleCylinder) as GameObject;
			} else {
				cylinder = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
			}
			cylinder.transform.parent = transform;

			Vector3 size = cylinder.GetComponent<Renderer>().bounds.size;

			Vector3 endPos = turtle.rotation * (Vector3.forward * moveDistance);
			endPos += turtle.position;

			cylinder.transform.position = (turtle.position + endPos) / 2 + transform.position;
			// multiply by Vector3.down to cancel out the starting rotation of the shape
			cylinder.transform.localRotation = turtle.rotation * Quaternion.LookRotation(Vector3.down);

			Vector3 scale = new Vector3(radius * 2 / size.x, moveDistance / size.y, radius * 2 / size.z);
			cylinder.transform.localScale = scale;
			Mesh mesh = cylinder.GetComponent<MeshFilter>().sharedMesh;
			vertexCount += mesh.vertices.Length;

			/* don't add to the combined mesh if the vertex count is over 65k
			* since that is the maximum vertex size of a mesh in Unity */
			if (vertexCount < 65000) {
				continue;
			}
			CombineInstance combine = new CombineInstance();
			combine.mesh = mesh;
			combine.transform = transform.worldToLocalMatrix
										* cylinder.transform.localToWorldMatrix;
			toCombine.Add(combine);
			Destroy(cylinder);
		}

		Mesh groupMesh = new Mesh();
		groupMesh.CombineMeshes(toCombine.ToArray());
		GetComponent<MeshFilter>().sharedMesh = groupMesh;
		if (GetComponent<MeshCollider>()) {
			GetComponent<MeshCollider>().sharedMesh = groupMesh;
		}
	}
}
