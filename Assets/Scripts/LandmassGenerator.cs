using UnityEngine;

public class LandmassGenerator : MonoBehaviour {

	public int mapWidth;
	public int mapHeight;
	public float noiseScale;
	public int ocatves;
	[Range(0, 1)]
	public float persistance;
	public float lacunarity;
	public Vector2 offset;

	public bool useFalloff = true;
	float[,] mFalloffMap;

	public float heightMultiplier;
	public AnimationCurve terrainHeightCurve;

	public void Awake() {
		mFalloffMap = FalloffGenerator.GenerateFalloffMap(mapWidth);
	}

	public void Start() {
		GenerateLandMass();

	}

	public void Update() {
		
	}

	public void GenerateLandMass() {
		float[,] noiseMap = Noise.GenerateNoiseMap(mapWidth,
													mapHeight,
													SeedManager.Seed,
													noiseScale,
													ocatves,
													persistance,
													lacunarity,
													offset
												);
		if (useFalloff) {
			for (int y = 0; y < mapHeight; y++) {
				for (int x = 0; x < mapWidth; x++) {
					noiseMap[x, y] -= mFalloffMap[x, y];
					noiseMap[x, y] = Mathf.Clamp01(noiseMap[x, y]);
					noiseMap[x, y] = terrainHeightCurve.Evaluate(noiseMap[x, y]);
				}
			}
		}
		Debug.Log("out tangent: " + terrainHeightCurve.keys[0].outTangent);
		Debug.Log("in tangent: " + terrainHeightCurve.keys[1].inTangent);

		MeshData meshData = MeshGenerator.GenerateTerrainMesh(noiseMap, heightMultiplier);
		meshData = MeshGenerator.SplitMeshDataTriangles(meshData);
		Mesh mesh = meshData.CreateMesh();
		GetComponent<MeshFilter>().sharedMesh = mesh;
		GetComponent<MeshCollider>().sharedMesh = mesh; 
		// Debug.Log("mesh has # vertices: " + meshData.vertices.Length);
		// Debug.Log("mesh has # triangles: " + meshData.triangles.Length);
	}

	void OnValidate() {
		if (mapWidth < 1) {
			mapWidth = 1;
		}
		if (mapHeight < 1) {
			mapHeight = 1;
		}
		if (lacunarity < 1) {
			lacunarity = 1;
		}
		if (ocatves < 0) {
			ocatves = 0;
		}
	}
}
