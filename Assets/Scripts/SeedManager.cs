using UnityEngine;

public class SeedManager {
	
	private static SeedManager _instance;
	private static SeedManager Instance {
		get {
			if (_instance == null) {
				_instance = new SeedManager();
			}
			return _instance;
		}	
	}

	private int _seed;
	private bool _unset = true;
	public static int Seed {
		get {
			if (Instance._unset) {
				return Mathf.RoundToInt(Random.value * 100000000f);
			}
			return Instance._seed;
		}
		set {
			if (!Instance._unset) {
				Debug.Log("seed has already been set");
				return;
			}
			Instance._seed = value;
			Instance._unset = false;
		}
	}

	public static void OverrideSeed(int seed) {
		Instance._seed = seed;
		Instance._unset = false;
	}
}