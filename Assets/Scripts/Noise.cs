using UnityEngine;

public static class Noise {

	public static float[,] GenerateNoiseMap(
												int mapWidth,
												int mapHeight,
												float scale) {
		return GenerateNoiseMap(mapWidth, mapHeight, 1, scale, 1, 1f, 1f, Vector2.zero);
	}

	public static float[,] SimpleNoiseMap(int mapWidth, int mapHeight, float scale) {
		float[,] noiseMap = new float[mapWidth,mapHeight];

		if (scale <= 0) {
			scale = 0.0001f;
		}

		for (int y = 0; y < mapHeight; y++) {
			for (int x = 0; x < mapWidth; x++) {
				float sampleX = x / scale;
				float sampleY = y / scale;

				Debug.Log("sampleX: " + sampleX + " sampleY: " + sampleY);

				float perlinValue = Mathf.PerlinNoise(sampleX, sampleY);
				noiseMap[x, y] = perlinValue;
				Debug.Log("noise value: " + noiseMap[x, y]);
			}
		}

		return noiseMap;
	}	

	public static float[,] GenerateNoiseMap(
											int mapWidth,
											int mapHeight,
											int seed,
											float scale,
											int octaves,
											float persistance,
											float lacunarity,
											Vector2 offset
										) {

		var noiseMap = new float[mapWidth,mapHeight];

		/* create array of seed values to control the generation */
		System.Random random = new System.Random(seed);
		Vector2[] offsets = new Vector2[octaves];
		for (int i = 0; i < octaves; i++) {
			var offsetX = random.Next(-10000, 100000) + offset.x;
			var offsetY = random.Next(-10000, 100000) + offset.y;
			offsets[i] = new Vector2(offsetX, offsetY);
		}

		if (scale <= 0) {
			scale = 0.0001f;
		}

		var minNoiseHeight = 0f;
		var maxNoiseHeight = 0f;

		var halfWidth = mapWidth/2;
		var halfHeight = mapHeight/2;

		for (int y = 0; y < mapHeight; y++) {
			for (int x = 0; x < mapWidth; x++) {

				var amplitude = 1f;
				var frequencey = 1f;
				var noiseHeight = 0f;

				for (int i = 0; i < octaves; i++) {

					var sampleX = (x-halfWidth) / scale * frequencey + offsets[i].x;
					var sampleY = (y-halfHeight) / scale * frequencey + offsets[i].y;

					/* adjust Perlin Noise to range -1 to 1 */
					var perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
					noiseHeight += perlinValue * amplitude;

					amplitude *= persistance;
					frequencey *= lacunarity;
				}

				if (noiseHeight > maxNoiseHeight) {
					maxNoiseHeight = noiseHeight;
				}
				else if (noiseHeight < minNoiseHeight) {
					minNoiseHeight = noiseHeight;
				}
				noiseMap [x, y] = noiseHeight;
			}
		}

		/* normalize all noiseMap values to the range 0 -> 1 */
		for (int y = 0; y < mapHeight; y++) {
			for (int x = 0; x < mapWidth; x++) {
				noiseMap[x, y] = Mathf.InverseLerp(
												minNoiseHeight,
												maxNoiseHeight,
												noiseMap[x, y]
											);
			}
		}

		return noiseMap;
	}

}