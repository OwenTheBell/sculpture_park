using UnityEngine;
using System.Collections.Generic;

public static class MeshGenerator {

	public static MeshData GenerateTerrainMesh(float[,] heightMap, float heightMultiplier) {
		int width = heightMap.GetLength(0);
		int height = heightMap.GetLength(1);

		float topLeftX = (width - 1) / -2f;
		float topLeftZ = (height - 1) / 2f;

		var meshData = new MeshData(width, height);
		int vertexIndex = 0;

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				meshData.vertices.Add(new Vector3(
												topLeftX + x,
												heightMap[x, y] * heightMultiplier,
												topLeftZ - y
											));
				meshData.uvs.Add(new Vector2(x/(float)width, y/(float)height));

				if (x < width -1 && y < height - 1) {
					meshData.AddTriangle(
										vertexIndex,
										vertexIndex + width + 1,
										vertexIndex + width
									);
					meshData.AddTriangle(
										vertexIndex + width + 1,
										vertexIndex,
										vertexIndex + 1
									);
				}
				vertexIndex++;
			}
		}

		return meshData;
	}

	public static MeshData GenerateSupershapeMehs(
													float a,
													float b,
													float[] n,
													int resolution = 100,
													float radius = 0f
												) {

		var outputMeshData = new MeshData();		

		var xPiStepValue = (2f * Mathf.PI) / resolution;
		var yPiStepValue = Mathf.PI / resolution;

		var radiusScalar = 1f;
		if (radius > 0) {
			var step = Mathf.PI * 2 / resolution;
			var maxRadius = 0f;
			for (var theta = 0f; theta < Mathf.PI * 2; theta += step) {
				var aux1 = Mathf.Pow(Mathf.Abs(Mathf.Cos(n[0]*theta/4)), n[2]);
				var aux2 = Mathf.Pow(Mathf.Abs(Mathf.Sin(n[0]*theta/4)), n[3]);
				var r = Mathf.Pow(aux1 + aux2, -(1/n[1]));
				if (r > maxRadius) {
					maxRadius = r;
				}
			}
			radiusScalar = radius / maxRadius;
		}

		var averageVertDistance = 0f;
		var furthestDistance = 0f;

		for (var u = -Mathf.PI; u <= Mathf.PI; u += xPiStepValue) {
			for (var v = -Mathf.PI / 2; v <= Mathf.PI/2; v += yPiStepValue) {
				if ((v >= Mathf.PI/2) && !(u >= Mathf.PI)) {
					Debug.Log("v: " + v + " u: " + u);
					continue;
				}
				var raux1 = Mathf.Pow(Mathf.Abs(Mathf.Abs(Mathf.Cos(n[0]*u / 4) / a)), n[2])
							+ Mathf.Pow(Mathf.Abs(Mathf.Abs(Mathf.Sin(n[0]*u / 4) / b)), n[3]);
				var r1 = Mathf.Pow(Mathf.Abs(raux1), -1/n[1]) * radiusScalar;
				var raux2 = Mathf.Pow(Mathf.Abs(Mathf.Abs(Mathf.Cos(n[0]*v / 4) / a)), n[2])
							+ Mathf.Pow(Mathf.Abs(Mathf.Abs(Mathf.Sin(n[0]*v / 4) / b)), n[3]);
				var r2 = Mathf.Pow(Mathf.Abs(raux2), -1/n[1]) * radiusScalar;
				var x = r1 * Mathf.Cos(u) * r2 * Mathf.Cos(v);
				var y = r1 * Mathf.Sin(u) * r2 * Mathf.Cos(v);
				var z = r2 * Mathf.Sin(v);
				var vertex = new Vector3(x, y, z);

				var distance = Vector3.Distance(vertex, Vector3.zero);
				if (u == -Mathf.PI && v == -Mathf.PI / 2) {
					averageVertDistance = distance;
				} else {
					averageVertDistance = (averageVertDistance + distance) / 2;
				}
				if (distance > furthestDistance) {
					furthestDistance = distance;
				}

				outputMeshData.vertices.Add(vertex);
			}
		}

		var lastRow = new List<int>();

		for (int x = 0; x < resolution; x++) {
			for (int y = 0; y < resolution - 1; y++) {
				if (x == resolution - 1) {
					var index1 = x * resolution + y;
					var index2 = x * resolution + y + 1;
					var index3 = y+1;
					var index4 = y;
					int[] points = {index3, index2, index1, index4, index3, index1};
					// int[] points = {idx1, idx2, idx3, idx1, idx3, idx4};
					for (int i = 0; i < points.Length; i++) {
						outputMeshData.triangles.Add(points[i]);
					}
					if (y == resolution - 2) {
						lastRow.Add(index2);
					}
				} else {
					var index1 = x * resolution + y;
					var index2 = x * resolution + y + 1;
					var index3 = (x+1) * resolution + y + 1;
					var index4 = (x+1) * resolution + y;
					int[] points = {index3, index2, index1, index4, index3, index1};
					// int[] points = {index1, index2, index3, index1, index3, index4};
					for (int i = 0; i < points.Length; i++) {
						outputMeshData.triangles.Add(points[i]);
					}
					if (y == resolution - 2) {
						lastRow.Add(index2);
					}
				}
			}
		}

		var lastVertex = outputMeshData.vertices.Count - 1;

		for (int i = 0; i < lastRow.Count - 1; i++) {
			int[] points = {lastRow[i], lastRow[i+1], lastVertex};
			for (int j = 0; j < points.Length; j++) {
				outputMeshData.triangles.Add(points[j]);
			}
		}

		outputMeshData.triangles.Add(lastVertex);
		outputMeshData.triangles.Add(lastRow[lastRow.Count - 1]);
		outputMeshData.triangles.Add(lastRow[0]);

		return outputMeshData;
	}

	public static MeshData SplitMeshDataTriangles(MeshData inputMeshData) {
		var outputMeshData = new MeshData();

		foreach (int index in inputMeshData.triangles) {
			outputMeshData.vertices.Add(inputMeshData.vertices[index]);
			outputMeshData.uvs.Add(inputMeshData.uvs[index]);
			outputMeshData.triangles.Add(outputMeshData.vertices.Count - 1);
		}

		return outputMeshData;
	}

	public static Mesh SplitMeshTriangles(Mesh inputMesh) {
		var outputMeshData = new MeshData();

		foreach (int index in inputMesh.triangles) {
			outputMeshData.vertices.Add(inputMesh.vertices[index]);
			if (index < inputMesh.uv.Length) {
				outputMeshData.uvs.Add(inputMesh.uv[index]);
			}
			outputMeshData.triangles.Add(outputMeshData.vertices.Count - 1);
		}

		return outputMeshData.CreateMesh();
	}
}

public class MeshData {
	public List<Vector3> vertices;
	public List<Vector2> uvs;
	public List<int> triangles;

	public MeshData() {
		vertices = new List<Vector3>();
		uvs = new List<Vector2>();
		triangles = new List<int>();
	}

	public MeshData(int width, int height) {
		vertices = new List<Vector3>(width * height);
		uvs = new List<Vector2>(width * height);
		triangles = new List<int>((width - 1) * (height - 1) * 6);
	}

	public void AddTriangle(int a, int b, int c) {
		triangles.Add(a);
		triangles.Add(b);
		triangles.Add(c);
	}

	public Mesh CreateMesh() {
		Mesh mesh = new Mesh();
		mesh.vertices = vertices.ToArray();
		mesh.uv = uvs.ToArray();
		mesh.triangles = triangles.ToArray();
		mesh.RecalculateNormals();
		return mesh;
	}
}