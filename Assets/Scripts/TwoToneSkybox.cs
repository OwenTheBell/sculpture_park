﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TwoToneSkybox : MonoBehaviour {

	public Color BaseColor = Color.black;
	public Color TopColor = Color.white;

	public int Width = 1024;
	public int Height = 1024;
	public float Radius = 1f;

	// Use this for initialization
	void Start () {
		Mesh mesh = GetComponent<MeshFilter>().mesh;
		List<Color> colors = new List<Color>();

		Vector2 tip = new Vector2(transform.position.x, transform.position.z);

		Vector3 highPoint = Vector3.zero;

		foreach (Vector3 vertex in mesh.vertices) {
			Vector3 point = transform.TransformPoint(vertex);
			if (highPoint.y < point.y) {
				highPoint = point;
			}
		}

		for (int i = 0; i < mesh.vertices.Length; i++) {
			float distance = Vector3.Distance(highPoint, transform.TransformPoint(mesh.vertices[i]));
			float percent = Easing.EaseIn(distance/Radius, 2);
			// colors[i] = VertexColor;
			colors.Add(Color.Lerp(TopColor, BaseColor, percent));
		}

		mesh.SetColors(colors);

		GetComponent<MeshFilter>().mesh = mesh;	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
