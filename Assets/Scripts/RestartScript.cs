using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartScript : MonoBehaviour {

	public KeyCode restartKey;

	void Update() {
		if (Input.GetKeyDown(restartKey)) {
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		}
	}
}
