using UnityEngine;

public class RandomOrientation : MonoBehaviour {

	public bool usePositionAsSeed = false;

	void Start() {
		var seedRandom = new System.Random((int)(Random.value * 100));
		if (usePositionAsSeed) {
			var seed = 0f;
			for (int i = 0; i < 3; i++) {
				seed += transform.position[i];
			}
			seed *= 100;
			seedRandom = new System.Random((int)seed);
		}

		var rotation = new Vector3();
		for (int i = 0; i < 3; i++) {
			rotation[i] = Mathf.Lerp(0f, 360f, (float)seedRandom.NextDouble());
		}

		transform.rotation = Quaternion.Euler(rotation);
	}

}
