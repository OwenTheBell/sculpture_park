using UnityEngine;

public class TestInvisbleScript : MonoBehaviour {

	void OnBeomeInvisible() {
		Debug.Log("no one can see me!");
	}

	void Update() {
		if (!GetComponent<Renderer>().isVisible) {
			Debug.Log("I can't be seen");
		}
	}
}
