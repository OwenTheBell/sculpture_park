using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(TurtleInterpreter))]
public class LSystemHandler : MonoBehaviour {

	public bool randomSystem = false;
	public Vector2 procedure_range = new Vector2(15f, 25f);
	public Vector2 F_range = new Vector2(5f, 10f);

	public int n;
	public char axiom = 'F';
	public string[] procedures;


	private Dictionary<string, List<char>> proceduresDict = new Dictionary<string, List<char>>();
	public Dictionary<string, List<char>> ProceduresDict {
		get {
			return proceduresDict;
		}
	}

	private char[] expandedSystem;
	public char[] ExpandedSystem{
		get {
			return expandedSystem;
		}
	}

	private TurtleInterpreter turtle;

	void Awake() {
		// RandomizedLSystem();
	}


	void Start () {
		turtle = GetComponent<TurtleInterpreter>();

		if (randomSystem) {
			ProceduresDict.Clear();
			var seed = 0f;
			for (int i = 0; i < 3; i++) {
				seed += transform.position[i];
			}
			seed *= 100;
			ProceduresDict.Add("F", RandomizedLSystem((int)seed));
		} else {
			ParseProcedures();
		}
		ExpandLSystem();

		turtle.InterpretLSystem(expandedSystem);
	}
	

	void Update () {
	
	}


	private List<char> RandomizedLSystem(int seed) {

		System.Random seedRandom = new System.Random(seed);

		int length = seedRandom.Next((int)procedure_range.x, (int)procedure_range.y);

		/*
		* first populate the procedure with Fs. This ensures that there are
		* at least a minimum amount in the procedure. The plants just don't look
		* good if there aren't enough Fs.
		*/
		List<char> newProcedure = new List<char>();
		int F_count = seedRandom.Next((int)F_range.x, (int)F_range.y);
		for (int i = 0; i < F_count; i++) {
			newProcedure.Add('F');
		}

		/*
		* Populate with [ ] pairs. This is done second to ensure that all pairs
		* include at least one F inside them.
		* The total number of brackets needs to be limited by the number of Fs in
		* the system or it is possible for an impossible number of brackets to be
		* fit into the system.
		*/
		int max_brackets = 4;
		if (newProcedure.Count <= 10) {
			max_brackets = Mathf.RoundToInt(newProcedure.Count / 3f);
		}

		int bracket_count = seedRandom.Next(2, max_brackets);
		for (int i = 0; i < seedRandom.Next(2, max_brackets); i++) {
			int index = seedRandom.Next(0, newProcedure.Count - 2);
			int checkBehind = (index > 0) ? index - 1 : 0;
			int checkAhead = index + 1;
			while (newProcedure[checkBehind] == '[' || newProcedure[checkAhead] == '[') {
				index++;
				checkAhead++;
				checkBehind++;
				if (index == newProcedure.Count - 1) {
					index = 0;
					checkBehind = 0;
					checkAhead = 1;
				}
			}
			newProcedure.Insert(index, '[');
		}
		for (int i = 0; i < newProcedure.Count - 1; i++) {
			if (newProcedure[i] == '[') {
				int close_index = seedRandom.Next(i + 2, newProcedure.Count - 1);
				while(newProcedure[close_index - 1] == '[') {
					close_index++;
				}
				newProcedure.Insert(close_index, ']');
			}
		}

		/*
		* insert operations between the characters.
		* Do not put any at the very end as they will have no effect on the 
		* interpretation.
		* Also ensure that all operations are not directly to the left of brackets
		* as then they will have no effect on the system.
		*/ 
		int operations_count = length - F_count - bracket_count;
		char[] operations = new char[]{'+', '-', '/', '\\', '&', '*'};
		for (int i = 0; i < operations_count; i++) {
			int index = seedRandom.Next(0, newProcedure.Count - 1);
			while (newProcedure[index] == ']' || newProcedure[index] == '[') {
				index++;
				if (index >= newProcedure.Count - 1) {
					index = 0;
				}
			}
			char operation = operations[seedRandom.Next(0, operations.Length - 1)];
			newProcedure.Insert(index, operation);
		}

		return newProcedure;
	}


	/*
	* break apart the procedures so they can be parsed later
	*/
	private void ParseProcedures() {
		proceduresDict.Clear();
		foreach (string tempProcedures in procedures) {
			string[] halves = tempProcedures.Split(new char[]{':'});
			halves[1] = halves[1].Trim();
			List<char> charList = new List<char>(halves[1].ToCharArray(0, halves[1].Length));
			proceduresDict.Add(halves[0].Trim(), charList);
		}
	}

	private void ExpandLSystem() {
		if (axiom.Equals(string.Empty)) {
			Debug.Log("this L system lacks an axiom");
			return;
		}
		List<char> outputList = new List<char>(){axiom};
		List<char> procedureList;
		// Debug.Log(characters);
		for (int i = 0; i < n; i++) {
			for (int j = outputList.Count - 1; j >= 0; j--) {
				string command = outputList[j] + "";
				if (proceduresDict.TryGetValue(command, out procedureList)) {
					outputList.RemoveAt(j);
					outputList.InsertRange(j, procedureList);
				}
			}
		}
		expandedSystem = outputList.ToArray();
	}


	public void CombineProcedureDicts(Dictionary<string, List<char>> dict1, Dictionary<string, List<char>> dict2) {
		proceduresDict.Clear();

		List<Vector2> subprocedures1;
		List<Vector2> subprocedures2;

		Dictionary<string, List<char>> newProcedures = new Dictionary<string, List<char>>();
		Dictionary<string, List<char>>.KeyCollection keys = dict1.Keys;

		List<char> procedure1;
		List<char> procedure2;
		List<Vector2> rangesSource;
		List<Vector2> rangesAlt;
		List<char> newProcedure;
		List<char> altProcedure;
		foreach (string key in keys) {
			if (dict1.TryGetValue(key, out procedure1) && dict2.TryGetValue(key, out procedure2)) {
				rangesSource = FindSubProcedures(procedure1);
				rangesAlt = FindSubProcedures(procedure2);

				if (Random.Range(0, 1) == 0) {
					newProcedure = procedure1;
					altProcedure = procedure2;
					rangesSource = FindSubProcedures(procedure1);
					rangesAlt = FindSubProcedures(procedure2);
				} else {
					newProcedure = procedure2;
					altProcedure = procedure1;
					rangesSource = FindSubProcedures(procedure2);
					rangesAlt = FindSubProcedures(procedure2);
				}

				Vector2 removeRange = rangesSource[Random.Range(0, rangesSource.Count)];
				Vector2 addRange = rangesAlt[Random.Range(0, rangesAlt.Count)];

				List<char> subProcedure = altProcedure.GetRange((int)addRange.x, (int)addRange.y);

				newProcedure.RemoveRange((int)removeRange.x, (int)removeRange.y);
				newProcedure.InsertRange((int)removeRange.x, subProcedure);

				Debug.Log(new string(newProcedure.ToArray()));

				proceduresDict.Add(key, newProcedure);
			}
		}

		ExpandLSystem();
		turtle.InterpretLSystem(expandedSystem);

	}


	List<Vector2> FindSubProcedures(List<char> procedure) {
		List<Vector2> substrings = new List<Vector2>();

		int counting = 0;
		int subIndex = 0, index = 0;

		foreach (char character in procedure) {
			if (character != '[' && character != ']') {
				if (counting <= 0) {
					subIndex = index;
				}
				counting++;
			} else if (counting > 0) {
				substrings.Add(new Vector2(subIndex, counting));
				counting = 0;
			}
			index++;
		}
		return substrings;
	}
}
