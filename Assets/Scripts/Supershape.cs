﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class Supershape : MonoBehaviour {

	public const float MIN_AB_RANGE = 0.01f;
	public const float MAX_AB_RANGE = 5f;
	public const float MIN_N_RANGE = 0.01f;
	public const float MAX_N_RANGE = 20f;

	public bool restrictRadius = false;
	public float fixedRadius = 1f;

	[SerializeField]
	private float a = 1;
	[SerializeField]
	private float b = 1;
	[SerializeField]
	private float n1 = 2;
	[SerializeField]
	private float n2 = 2;
	[SerializeField]
	private float n3 = 2;
	[SerializeField]
	private float n4 = 2;

	private float hue = 0.5f;
	private float saturation = 1;
	private float value = 0.06f;

	public float stepValue;
	[Range(20, 200)]
	public int resolution;

	public bool recalculateNormals;

	private MeshRenderer meshRenderer;

    private SphereCollider mSphereCollider;

    private List<int> lastRow = new List<int>();
	private int lastVertex = -1;

	private bool redraw = false;
	private bool selected = false;

	private const float GROW_SPEED = 2f;

	public float A {
		get {
			return a;
		}
		set {
			a = Mathf.Clamp(value, MIN_AB_RANGE, MAX_AB_RANGE);
			redraw = true;
		}
	}

	public float B {
		get {
			return b;
		}
		set {
			b = Mathf.Clamp(value, MIN_AB_RANGE, MAX_AB_RANGE);
			redraw = true;
		}
	}

	public float N1 {
		get {
			return n1;
		}
		set {
			n1 = Mathf.Clamp(value, MIN_N_RANGE, MAX_N_RANGE);
			redraw = true;
		}
	}

	public float N2 {
		get {
			return n2;
		}
		set {
			n2 = Mathf.Clamp(value, MIN_N_RANGE, MAX_N_RANGE);
			redraw = true;
		}
	}

	public float N3 {
		get {
			return n3;
		}
		set {
			n3 = Mathf.Clamp(value, MIN_N_RANGE, MAX_N_RANGE);
			redraw = true;
		}
	}

	public float N4 {
		get {
			return n4;
		}
		set {
			n4 = Mathf.Clamp(value, MIN_N_RANGE, MAX_N_RANGE);
			redraw = true;
		}
	}

	public Vector2 abRange {
		get {
			return new Vector2(MIN_AB_RANGE, MAX_AB_RANGE);
		}
	}

	public Vector2 nRange {
		get {
			return new Vector2(MIN_N_RANGE, MAX_N_RANGE);
		}
	}

	void Awake() {
		meshRenderer = GetComponent<MeshRenderer>();
	}

	// Use this for initialization
	void Start () {
		CalculateMesh();
	}

	// Update is called once per frame
	void Update () {
	}

	void LateUpdate() {
		if (redraw) {
			CalculateMesh();
			redraw = false;
		}
	}

	void CalculateMesh() {
		float time = Time.realtimeSinceStartup;

		var mesh = new Mesh();

		// var xResolution = (int) Mathf.Ceil(2.0f * Mathf.PI / stepValue);
		// var yResolution = (int) Mathf.Ceil(Mathf.PI / stepValue);

		var xPiStepValue = (2f * Mathf.PI) / resolution;
		var yPiStepValue = Mathf.PI / resolution;


		var radiusScalar = 1f;
		if (restrictRadius) {
			var step = Mathf.PI * 2 / resolution;
			var maxRadius = 0f;
			for (var theta = 0f; theta < Mathf.PI * 2; theta += step) {
				var aux1 = Mathf.Pow(Mathf.Abs(Mathf.Cos(n1*theta/4)), n3);
				var aux2 = Mathf.Pow(Mathf.Abs(Mathf.Sin(n1*theta/4)), n4);
				var r = Mathf.Pow(aux1 + aux2, -(1/n1));
				if (r > maxRadius) {
					maxRadius = r;
				}
			}
			radiusScalar = fixedRadius / maxRadius;
		}

		var newVertices = new List<Vector3>();

		var averageVertDistance = 0f;
		var furthestDistance = 0f;

		for (var u = -Mathf.PI; u <= Mathf.PI; u += xPiStepValue) {
			for (var v = -Mathf.PI / 2; v <= Mathf.PI/2; v += yPiStepValue) {
				if ((v >= Mathf.PI/2) && !(u >= Mathf.PI)) {
					Debug.Log("v: " + v + " u: " + u);
					continue;
				}
				var raux1 = Mathf.Pow(Mathf.Abs(Mathf.Abs(Mathf.Cos(n1*u / 4) / a)), n3)
							+ Mathf.Pow(Mathf.Abs(Mathf.Abs(Mathf.Sin(n1*u / 4) / b)), n4);
				var r1 = Mathf.Pow(Mathf.Abs(raux1), -1/n2) * radiusScalar;
				var raux2 = Mathf.Pow(Mathf.Abs(Mathf.Abs(Mathf.Cos(n1*v / 4) / a)), n3)
							+ Mathf.Pow(Mathf.Abs(Mathf.Abs(Mathf.Sin(n1*v / 4) / b)), n4);
				var r2 = Mathf.Pow(Mathf.Abs(raux2), -1/n2) * radiusScalar;
				var x = r1 * Mathf.Cos(u) * r2 * Mathf.Cos(v);
				var y = r1 * Mathf.Sin(u) * r2 * Mathf.Cos(v);
				var z = r2 * Mathf.Sin(v);
				var vertex = new Vector3(x, y, z);

				var distance = Vector3.Distance(vertex, Vector3.zero);
				if (u == -Mathf.PI && v == -Mathf.PI / 2) {
					averageVertDistance = distance;
				} else {
					averageVertDistance = (averageVertDistance + distance) / 2;
				}
				if (distance > furthestDistance) {
					furthestDistance = distance;
				}

				newVertices.Add(vertex);
			}
		}

        var triangles = new List<int>();
		lastRow.Clear();

		for (int x = 0; x < resolution; x++) {
			for (int y = 0; y < resolution - 1; y++) {
				if (x == resolution - 1) {
					var index1 = x * resolution + y;
					var index2 = x * resolution + y + 1;
					var index3 = y+1;
					var index4 = y;
					int[] points = {index3, index2, index1, index4, index3, index1};
					// int[] points = {idx1, idx2, idx3, idx1, idx3, idx4};
					for (int i = 0; i < points.Length; i++) {
						triangles.Add(points[i]);
					}
					if (y == resolution - 2) {
						lastRow.Add(index2);
					}
				} else {
					var index1 = x * resolution + y;
					var index2 = x * resolution + y + 1;
					var index3 = (x+1) * resolution + y + 1;
					var index4 = (x+1) * resolution + y;
					int[] points = {index3, index2, index1, index4, index3, index1};
					// int[] points = {index1, index2, index3, index1, index3, index4};
					for (int i = 0; i < points.Length; i++) {
						triangles.Add(points[i]);
					}
					if (y == resolution - 2) {
						lastRow.Add(index2);
					}
				}
			}
		}

		lastVertex = newVertices.Count - 1;

		for (int i = 0; i < lastRow.Count - 1; i++) {
			int[] points = {lastRow[i], lastRow[i+1], lastVertex};
			for (int j = 0; j < points.Length; j++) {
				triangles.Add(points[j]);
			}
		}

		triangles.Add(lastVertex);
		triangles.Add(lastRow[lastRow.Count - 1]);
		triangles.Add(lastRow[0]);

		mesh.vertices = newVertices.ToArray();
		mesh.triangles = triangles.ToArray();

		if (recalculateNormals) {
			mesh.RecalculateNormals();
		}
		mesh.RecalculateBounds();

		GetComponent<MeshFilter>().sharedMesh = mesh;
		if (GetComponent<MeshCollider>()) {
			GetComponent<MeshCollider>().sharedMesh = mesh;
		}
	}

	void SetAllValue(float na, float nb, float nn1, float nn2, float nn3, float nn4) {
		A = na;
		B = nb;
		N1 = nn1;
		N2 = nn2;
		N3 = nn3;
		N4 = nn4;
	}

	void OnDrawGizmos() {
		// Gizmos.color = Color.magenta;
		// for (int i = 0; i < lastRow.Count; i++) {
		// 	if (lastRow[i] < mesh.vertices.Length) {
		// 		Vector3 vertex = mesh.vertices[lastRow[i]];
		// 		vertex = transform.TransformPoint(vertex);
		// 		Gizmos.DrawSphere(vertex, 0.1f);
		// 	}
		// }
		// if (lastVertex > -1) {
		// 	Gizmos.color = Color.green;
		// 	Vector3 vertex = transform.TransformPoint(mesh.vertices[lastVertex]);
		// 	Gizmos.DrawSphere(vertex, 0.1f);
		// }
		// Gizmos.color = Color.magenta;
		// if (mesh) {
		// 	for (int i = 0; i < mesh.vertices.Length; i++) {
		// 		Vector3 vertex = mesh.vertices[i];
		// 		vertex = transform.TransformPoint(vertex);
		// 		Gizmos.DrawSphere(vertex, 0.01f);
		// 	}
		// }
	}
}
