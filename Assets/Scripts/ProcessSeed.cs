using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class ProcessSeed : MonoBehaviour {

	private bool _loading = false;

	void Update() {
		if (Input.GetKeyDown(KeyCode.Return) && !_loading) {
			var seedString = GetComponent<Text>().text;
			seedString = seedString.ToLower();
			var seed = 0;
			foreach (char c in seedString) {
				seed += c;
			}
			SeedManager.OverrideSeed(seed);

			if (seedString == "nothing" || seedString == "quit" || seedString == "exit") {
				Application.Quit();
			}

			StartCoroutine(LoadScene());
		}
	}

	private IEnumerator LoadScene() {
		_loading = true;
		var operation = SceneManager.LoadSceneAsync("Main Scene", LoadSceneMode.Additive);
		yield return operation;
		SceneManager.UnloadScene("Input Scene");
		yield return null;
	}
}
