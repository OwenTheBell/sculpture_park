using UnityEngine;

public class SculpturePlacer : MonoBehaviour {

	public int width, height;
	public int count;
	public Vector2 CountRange;

	public GameObject[] prefabs;

	public LayerMask layerMask;

	public void Start() {
		var seededRandom = new System.Random(SeedManager.Seed);
		var sculpture = seededRandom.Next((int)CountRange.x, (int)CountRange.y);
		Vector3[] points = PointDistributer.Mitchells(width, height, sculpture, SeedManager.Seed);

		foreach (Vector3 point in points) {
			Vector3 origin = point;
			origin.y = 100f;
			//var seed = 0f;
			//for (int i = 0; i < 3; i++) {
			//	seed += origin[i];
			//}
			//seed *= 100;
			//System.Random seededRandom = new System.Random((int)seed);
			RaycastHit hit;
			if (Physics.Raycast(origin, Vector3.down, out hit, 100f, layerMask)) {
				if (hit.point.y < 0.05f) {
					continue;
				}
				int index = seededRandom.Next(0, prefabs.Length);
				GameObject sphere = (GameObject)Instantiate(prefabs[index]);
				sphere.transform.position = hit.point;
				sphere.transform.parent = transform;
			}

		}
	}
}
