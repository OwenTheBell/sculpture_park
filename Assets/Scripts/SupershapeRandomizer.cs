﻿using UnityEngine;
using System.Collections;

public class SupershapeRandomizer : MonoBehaviour {

	public Vector2 abRange;
	public Vector2 n1Range;
	public Vector2 n2Range;
	public Vector2 n3Range;
	public Vector2 n4Range;
	public Vector2 radiusRange;
	[Range(20, 200)]
	public int resolution;

	void Start () {
		var seed = 0f;
		for (int i = 0; i < 3; i++) {
			seed += transform.position[i];
		}
		seed *= 100;
		System.Random seededRandom = new System.Random((int)seed);
		Supershape supershape = GetComponent<Supershape>();

		float[] n = new float[4];

		float next = (float)seededRandom.NextDouble();
		var a = Mathf.Lerp(abRange.x, abRange.y, next);
		next = (float)seededRandom.NextDouble();
		var b = Mathf.Lerp(abRange.x, abRange.y, next);
		n[0] = Mathf.Lerp(n1Range.x, n1Range.y, next);
		next = (float)seededRandom.NextDouble();
		n[1] = Mathf.Lerp(n2Range.x, n2Range.y, next);
		next = (float)seededRandom.NextDouble();
		n[2] = Mathf.Lerp(n3Range.x, n3Range.y, next);
		next = (float)seededRandom.NextDouble();
		n[3] = Mathf.Lerp(n4Range.x, n4Range.y, next);
		float percent = (float)seededRandom.NextDouble();
		float easeIn = Easing.EaseIn(percent, 2);
		float radius = Mathf.Lerp(radiusRange.x, radiusRange.y, easeIn);

		MeshData meshData = MeshGenerator.GenerateSupershapeMehs(a, b, n, resolution, radius);

		GetComponent<MeshFilter>().sharedMesh = meshData.CreateMesh();
		if (GetComponent<MeshCollider>()) {
			GetComponent<MeshCollider>().sharedMesh = meshData.CreateMesh();
		}

		Vector3 position = transform.position;
		position.y += radius + Random.Range(0f, radius);
		transform.position = position;	
	}
}
